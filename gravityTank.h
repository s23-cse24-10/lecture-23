#ifndef GRAVITY_TANK
#define GRAVITY_TANK

#include <iostream>
#include <ostream>
#include <stdexcept>

template <class T>
struct GravityTank {
private:
    T* storage;
    int count;
    int capacity;

public:
    // default constructor
    GravityTank() {
        count = 0;
        capacity = 1;
        storage = new T[capacity];
    }

    GravityTank(const GravityTank<T>& other) {
        count = other.count;
        capacity = other.capacity;
        storage = new T[capacity];

        for (int i = 0; i < count; i++) {
            storage[i] = other.storage[i];
        }
    }

    GravityTank<T> operator=(const GravityTank<T>& other) {
        if (capacity == other.capacity) {
            count = other.count;

            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        } else {
            delete[] storage;
            count = other.count;
            capacity = other.capacity;
            storage = new T[capacity];

            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        }
        return *this;
    }

    bool operator>(const GravityTank<T>& other) {
        return count > other.count;
    }

    // append function
    void append(T x) {
        // append item to last position
        storage[count] = x;
        count++;

        // check if capacity needs to be doubled
        if (count == capacity) {
            capacity *= 2;

            T* temp = new T[capacity];
            for (int i = 0; i < count; i++) {
                temp[i] = storage[i];
            }

            T* old = storage;
            storage = temp;
            delete[] old;
        }

        // check if last appended item is in the correct poisition
        int curr = count - 1;
        while (curr > 0 && storage[curr - 1] > storage[curr]) {
            T temp = storage[curr];
            storage[curr] = storage[curr - 1];
            storage[curr - 1] = temp;
            curr--;
        }
    }

    // destructor
    ~GravityTank() {
        delete[] storage;
    }

    int getCount() const {
        return count;
    }

    int getCapacity() const {
        return capacity;
    }

    T getItem(int index) const {
        return storage[index];
    }

    T remove() {
        if (count > 0) {
            T item = storage[0];

            for (int i = 1; i < count; i++) {
                storage[i - 1] = storage[i];
            }
            count--;

            if ((capacity /2) > count) {
                capacity /= 2;

                T* temp = new T[capacity];
                for (int i = 0; i < count; i++) {
                    temp[i] = storage[i];
                }

                T* old = storage;
                storage = temp;
                delete[] old;

            }


            return item;
        }
        throw std::out_of_range("Gravity tank is empty");
    }

    // template<class Type>
    // friend std::ostream& operator<<(std::ostream&, const GravityTank<Type>&);
};

// overloaded shift left operator
template <class T>
std::ostream& operator<<(std::ostream& os, const GravityTank<T>& tank) {
    os << "capacity: " << tank.getCapacity() <<  " : Gravity tank: ";
    for (int i = 0; i < tank.getCount(); i++) {
        os << tank.getItem(i);
        if (i < tank.getCount() - 1) {
            os << ", ";
        }
    }

    return os;
}

#endif