#include <cstdint>
#include <iostream>
#include "gravityTank.h"
using namespace std;

int main() {
    GravityTank<int> tank;
    tank.append(5);
    tank.append(7);
    tank.append(3);
    cout << tank << endl << endl;

    cout << "remove(): " << tank.remove() << endl;
    cout << tank << endl << endl;

    cout << "remove(): " << tank.remove() << endl;
    cout << tank << endl << endl;

    cout << "remove(): " << tank.remove() << endl;
    cout << tank << endl << endl;

    // cout << "remove(): " << tank.remove() << endl;
    // cout << tank << endl << endl;


    return 0;
}